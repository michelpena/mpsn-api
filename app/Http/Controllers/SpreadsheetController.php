<?php

namespace App\Http\Controllers;

use App\Spreadsheet;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Jobs\SpreadsheetProcessing;

class SpreadsheetController extends Controller
{

    public function create(Request $request)
    {
        $this->validate($request, array(
            'file'   => 'max:10240|required|mimes:csv,xlsx',
        ));
        $file = $request->file('file');

        $path = $file->getRealPath();

        $obj = new Spreadsheet();
        $obj->filename = $file->getClientOriginalName();
        $obj->content_type = $file->getClientMimeType();
        $obj->content = base64_encode(file_get_contents($path));
        $obj->status_id = Spreadsheet::STATUS_QUEUED;

        $obj->save();
        $spreadsheet = Spreadsheet::find($obj->id);

        $this->dispatch(new SpreadsheetProcessing($spreadsheet));

        return response()->json($spreadsheet, 201);
    }

    public function changeStatus($id, $status_id)
    {
        $spreadsheet = Spreadsheet::find($id);
        $spreadsheet->status_id = $status_id;
        $spreadsheet->save();

        return;
    }

    public function checkProcessing($id)
    {
        $spreadsheet = Spreadsheet::find($id);
        $statusProcessing = Spreadsheet::fetchDescriptionProcessing($spreadsheet->status_id);

        return response()->json(['processing_status' => $statusProcessing['description']]);
    }
}