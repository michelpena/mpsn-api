<?php

namespace App\Http\Controllers;

use App\Product;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function index()
    {
        return response()->json(Product::all());
    }

    public function get($id)
    {
        return response()->json(Product::find($id));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
            'im' => 'required|integer',
            'free_shipping' => 'required|boolean',
            'price' => "required|regex:/^\d*(\.\d{1,2})?$/"
        ]);

        $product  = Product::find($id);
        $product->im = $request->input('im');
        $product->name = $request->input('name');
        $product->free_shipping = $request->input('free_shipping');
        $product->description = $request->input('description');
        $product->price = $request->input('price');
        $product->save();
  
        return response()->json($product);
    }

    public function delete($id)
    {
        Product::findOrFail($id)->delete();
        return response('Deleted Successfully', 200);
    }
}
