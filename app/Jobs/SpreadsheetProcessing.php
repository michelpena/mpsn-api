<?php

namespace App\Jobs;

use App\Http\Controllers\SpreadsheetController;
use App\Spreadsheet;
use App\Product;

use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;

use Excel;

class SpreadsheetProcessing extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    public $spreadsheet;

    public function __construct($spreadsheet)
    {
        $this->spreadsheet = $spreadsheet;
    }

    public function handle()
    {
        $spreadsheetController = (new SpreadsheetController);
        $spreadsheetController->changeStatus($this->spreadsheet->id, Spreadsheet::STATUS_EXECUTING);

        config(['excel.import.startRow' => 4 ]);

        $content = base64_decode($this->spreadsheet->content);
        $name_spreadsheet = $this->spreadsheet->id.'-'.$this->spreadsheet->filename;
        file_put_contents($name_spreadsheet, $content, FILE_APPEND | LOCK_EX);
        $data = Excel::load($name_spreadsheet, function($reader) {$reader->noHeading = true;})->get();

        foreach ($data as $row) {
            foreach ($row as $res) {
                Product::firstOrCreate(['im' => $res[0],
                                        'name' => $res[1], 
                                        'free_shipping' => $res[2], 
                                        'description' => $res[3],
                                        'price' => $res[4],
                                        ]);
            }
        }

        $spreadsheetController->changeStatus($this->spreadsheet->id, Spreadsheet::STATUS_FINISHED);
        unlink($name_spreadsheet);
    }
}