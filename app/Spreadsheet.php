<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Spreadsheet extends Model
{
    const STATUS_QUEUED = 1;
    const STATUS_EXECUTING = 2;
    const STATUS_FINISHED = 3;
    const STATUS_FAILED = 4;

    protected $data = [
        ['id' => 1, 'description' => 'queued'],
        ['id' => 2, 'description' => 'executing'],
        ['id' => 3, 'description' => 'finished'],
        ['id' => 4, 'description' => 'failed'],
    ];
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'filename', 'content_type', 'content', 'status_id'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    protected function fetchDescriptionProcessing($id)
    {
        return $this->getOptionById($id, $this->data);
    }

    protected function getOptionById($id, array $options)
    {
        if (! $this->hasOptionById($id, $options)) {
            throw new Exception("ID não encontrado: $id");
        }

        $arrayKey = $this->getOptionArrayKeyById($id, $options);

        return $options[$arrayKey];
    }

    protected function hasOptionById($id, array $options)
    {
        if (! empty($options)) foreach ($options as $arrayKey => $option) {
            foreach ($option as $key => $value) {
                if ('id' == $key && $id == $value) {
                    return true;
                }
            }
        }

        return false;
    }

    protected function getOptionArrayKeyById($id, array $options)
    {
        if (! empty($options)) foreach ($options as $arrayKey => $option) {
            foreach ($option as $key => $value) {
                if ('id' == $key && $id == $value) {
                    return $arrayKey;
                }
            }
        }

        return [];
    }
}
