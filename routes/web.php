<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => 'api'], function () use ($router) {
    $router->get('product',  ['uses' => 'ProductController@index']);
    $router->get('product/{id}', ['uses' => 'ProductController@get']);
    $router->put('product/{id}', ['uses' => 'ProductController@update']);
    $router->delete('product/{id}', ['uses' => 'ProductController@delete']);

    $router->post('spreadsheet', ['uses' => 'SpreadsheetController@create']);
    $router->get('check-spreadsheet_processign/{id}', ['uses' => 'SpreadsheetController@checkProcessing']);

});
