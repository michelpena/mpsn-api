# mpsn-api
Instalação:
Pre-requisitos:
	- PHP >= 7.1.3
	- OpenSSL PHP Extension
	- PDO PHP Extension
	- Mbstring PHP Extension
	- php-zip Extension

criar o banco: mpsn-api

git https://bitbucket.org/michelpena/mpsn-api
cd mpsn-api
cp .env.example .env
ajustes do DB_CONNECTION=mysql
composer install
php artisan migrate

Subir o server: 
php -S localhost:8000 -t public

Subir a execução da fila:
php artisan queue:listen

End-points:

- Enviar xlsx: 
Método: POST
url: http://localhost:8000/api/spreadsheet
Body:
Key: file
value: arquivo.xlsx

- Status Processsamento da planilha:
Método: GET
url: http://localhost:8000/api/check-spreadsheet_processign/ID_DA_SPREADSHEET
* ID_DA_SPREADSHEET =  é retornado no post que envia o xlsx

- Listagem de produtos:
Método: GET
url: http://localhost:8000/api/product

- Detalhe de um produto
Método: GET
url: http://localhost:8000/api/product/ID_DO_PRODUTO

- Atualizar produto
Método: PUT
url: http://localhost:8000/api/product/ID_DO_PRODUTO
Headers:
Key = Content-Type
Value = application/x-www-form-urlencoded
Body:
name = XXXX
im = 123
description = descrição
price = 156.20
free_shipping = 0

- deletar produto
Método: DELETE
url: http://localhost:8000/api/product/ID_DO_PRODUTO
